# Produire un mémoire numérique
Article présentant le _making-of_ de la réalisation du mémoire [Vers un système modulaire de publication : éditer avec le numérique](https://memoire.quaternum.net) pour le carnet [DLIS](https://dlis.hypotheses.org/).
